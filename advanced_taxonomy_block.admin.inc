<?php

/**
 * @file
 * Functionality for Nice Menus administration.
 */

/**
 * Settings form as implemented by hook_menu.
 */
function advanced_taxonomy_block_admin_settings($form, &$form_state) {
  // Number of Nice Menus blocks.
  $form['advanced_taxonomy_block_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of advanced taxonomy block'),
    '#description' => t('The total number of independent taxonomy blocks you want. Enter a number between 0 and 10. If you set this to 0, you will have no blocks created but you can still use the Nice menus theme functions directly in your theme.'),
    '#default_value' => variable_get('advanced_taxonomy_block_number', 2),
    '#size' => 2,
    '#title_display' => 'invisible',
    '#maxlength' => 2,
  );
  return system_settings_form($form);
}

/**
 * Custom validation for the settings form.
 */
function advanced_taxonomy_block_settings_validate($form, &$form_state) {
  $number = $form_state['values']['advanced_taxonomy_block_number'];
  // Check to make sure it is a number and that is a maximum of 2 digits.
  if (!is_numeric($number) || strlen($number) > 2) {
    form_set_error('advanced_taxonomy_block_number', t('You must enter a number from 0 to 10.'));
  }
}
